package com.tools.util;

/**
 * @ClassName: SortUtil
 * @Description: 各排序算法整合工具类
 * @author: zouyp02
 * @date: 2018-11-26 15:49:17
 */
public class SortUtil {

	/**
	 * 冒泡排序【小到大】
	 */
	public static int[] bubbleSort(int[] arr) {
		// 外层循环控制排序趟数
		for (int i = 0; i < arr.length - 1; i++) {
			// 内层循环控制每一趟排序多少次
			for (int j = 0; j < arr.length - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {
					int temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}
		return arr;
	}

	/**
	 * 快速排序【小到大】
	 * 
	 * @return
	 */
	public static int[] quickSort(int[] arr, int low, int high) {
		int i, j, temp, t;
		if (low > high) {
			return arr;
		}
		i = low;
		j = high;
		// temp就是基准位
		temp = arr[low];

		while (i < j) {
			// 先看右边，依次往左递减
			while (temp <= arr[j] && i < j) {
				j--;
			}
			// 再看左边，依次往右递增
			while (temp >= arr[i] && i < j) {
				i++;
			}
			// 如果满足条件则交换
			if (i < j) {
				t = arr[j];
				arr[j] = arr[i];
				arr[i] = t;
			}
		}
		// 最后将基准为与i和j相等位置的数字交换
		arr[low] = arr[i];
		arr[i] = temp;
		// 递归调用左半数组
		quickSort(arr, low, j - 1);
		// 递归调用右半数组
		quickSort(arr, j + 1, high);
		return arr;
	}

	/**
	 * 直接插入排序【小到大】
	 */
	public static int[] insertSort(int[] arr) {
		int n = arr.length;
		int i, j;
		for (i = 1; i < n; i++) {
			// temp为本次循环待插入有序列表中的数
			int temp = arr[i];
			// 寻找temp插入有序列表的正确位置
			for (j = i - 1; j >= 0 && arr[j] > temp; j--) {
				// 元素后移，为插入temp做准备
				arr[j + 1] = arr[j];
			}
			// 插入temp
			arr[j + 1] = temp;
		}

		return arr;
	}
	
	/**   
	 * 插入排序-希尔排序【小到大】
	 */
	public static int[] hillSort(int[] arr) {
		// i表示希尔排序中的第n/2+1个元素（或者n/4+1）
		// j表示希尔排序中从0到n/2的元素（n/4）
		// r表示希尔排序中n/2+1或者n/4+1的值
		int i, j, r, tmp;
		// 划组排序
		for (r = arr.length / 2; r >= 1; r = r / 2) {
			for (i = r; i < arr.length; i++) {
				tmp = arr[i];
				j = i - r;
				// 一轮排序
				while (j >= 0 && tmp < arr[j]) {
					arr[j + r] = arr[j];
					j -= r;
				}
				arr[j + r] = tmp;
			}
		}
		return arr;
	}
	
	/**   
	 * 选择排序
	 */
	public static int[] selectSort(int[] arr) {
		int n = arr.length;
		for (int i = 0; i < n - 1; i++) {
			int index = i;
			int j;
			// 找出最小值得元素下标
			for (j = i + 1; j < n; j++) {
				if (arr[j] < arr[index]) {
					index = j;
				}
			}
			int tmp = arr[index];
			arr[index] = arr[i];
			arr[i] = tmp;
		}
		return arr;

	}

	public static void main(String[] args) {
		int[] arr = { 6, 3, 8, 2, 9, 1 };
		System.out.print("排序前数组为： ");
		for (int i : arr) {
			System.out.print(i + " ");
		}

		int[] bubbleArr = bubbleSort(arr);
		System.out.print("\n冒泡排序后数组为： ");
		for (int i : bubbleArr) {
			System.out.print(i + " ");
		}

		int[] quickArr = quickSort(arr, 0, arr.length - 1);
		System.out.print("\n快速排序后数组为： ");
		for (int i : quickArr) {
			System.out.print(i + " ");
		}

		int[] insertArr = insertSort(arr);
		System.out.print("\n直接插入排序后数组为： ");
		for (int i : insertArr) {
			System.out.print(i + " ");
		}
		
		int[] hillArr = insertSort(arr);
		System.out.print("\n希尔排序后数组为： ");
		for (int i : hillArr) {
			System.out.print(i + " ");
		}
		
		int[] selectArr = selectSort(arr);
		System.out.print("\n选择排序后数组为： ");
		for (int i : selectArr) {
			System.out.print(i + " ");
		}
	}
}
