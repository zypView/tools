package com.tools.util;

import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/**
 * 随机数工具类
 */
public class RandomUtil {
	/**
	 * 生成n位随机数
	 * 
	 * @param n
	 *          随机数的长度
	 * @return 生成的随机数
	 */
	public static long getNDigitRanNum(int n) {
		if (n < 1) {
			throw new IllegalArgumentException("随机数位数必须大于0");
		}
		return (long) (Math.random() * 9 * Math.pow(10, n - 1)) + (long) Math.pow(10, n - 1);
	}

	/**
	 * 生成指定上下界的随机数 [0, maxBound)%range=[0, range)+minBound=[minBound,
	 * minBound+range)
	 * 
	 * @param minBound
	 *          下界
	 * @param maxBound
	 *          上界
	 */
	public static int getRandomNumInRange(int minBound, int maxBound) {
		Random random = new Random();
		return random.nextInt(maxBound) % (maxBound - minBound + 1) + minBound;
	}

	/**
	 * events:key(事件):value(概率)，用TreeMap保持加入的顺序 0.4,0.4,0.2 400,800,1000
	 * 
	 * @param events
	 *          随机事件
	 */
	public static String probabilityEvent(TreeMap<String, Double> events) {
		double p_total = 1.0;
		int random = getRandomNumInRange(0, 1000); // 取出随机范围为0~999的一个随机数
		int index = 0;
		String result = "";
		for (Map.Entry<String, Double> event : events.entrySet()) {
			p_total = p_total - event.getValue(); // 用于检查事件总概率小于1
			if ("".equals(event.getKey().trim()) || event.getValue() <= 0.0 || event.getValue() >= 1.0) {
				throw new IllegalArgumentException("事件不得为空，事件概率不能小于0大于1");
			}
			int range = (int) (event.getValue() * 1000); // 计算出该概率在1000中所占的范围
			index = index + range;
			// 用""来控制不重复获取事件
			if ("".equals(result) && random < index) {
				result = event.getKey();
			}
		}
		if (p_total < 0.0) {
			throw new IllegalArgumentException("事件概率相加必须小于1");
		}
		return result;
	}
}
