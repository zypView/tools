package com.tools.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 */
public class DateUtil {

	/**
	 * 时间格式化
	 * 
	 * @param date
	 *          需要格式化的时间
	 * @param pattern
	 *          转化格式，例yyyy-MM-dd HH:mm:ss || yyyy-MM-dd || yyyy/MM/dd || yyyyMMdd
	 *          || HH:mm:ss
	 */
	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}

	/**
	 * 计算两个日期间的月份数(包括首尾月)
	 */
	public static int getMonthOfDate(Calendar startCal, Calendar endCal) {
		int startYear = startCal.get(Calendar.YEAR);
		int startMonth = startCal.get(Calendar.MONTH);
		int endYear = endCal.get(Calendar.YEAR);
		int endMonth = endCal.get(Calendar.MONTH);
		int month = (endYear * 12 + endMonth) - (startYear * 12 + startMonth) + 1;
		return month;
	}

	/**
	 * 获取前一天日期yyyyMMdd 经测试，针对闰年02月份或跨年等情况，该代码仍有效。测试代码如下
	 * calendar.set(Calendar.YEAR, 2013); calendar.set(Calendar.MONTH, 0);
	 * calendar.set(Calendar.DATE, 1); 测试时，将其放到
	 * <code>calendar.add(Calendar.DATE, -1);</code>前面即可
	 * 
	 * @return 返回的日期格式为yyyyMMdd
	 */
	public static String getYestoday() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
	}

	/**
	 * 获取当前的日期yyyyMMdd
	 */
	public static String getCurrentDate() {
		return new SimpleDateFormat("yyyyMMdd").format(new Date());
	}

	/**
	 * 获取当前的时间yyyyMMddHHmmss
	 */
	public static String getCurrentTime() {
		return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	}

}
