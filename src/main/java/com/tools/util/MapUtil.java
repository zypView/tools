package com.tools.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Map工具类
 */
public class MapUtil {
	/**
	 * 获取Map中的属性 由于Map.toString()打印出来的参数值对,是横着一排的...参数多的时候,不便于查看各参数值
	 * 故此仿照commons-lang.jar中的ReflectionToStringBuilder.toString()编写了本方法
	 * 
	 * @return String key11=value11 \n key22=value22 \n key33=value33 \n......
	 */
	public static String getStringFromMap(Map<String, String> map) {
		StringBuilder sb = new StringBuilder();
		sb.append(map.getClass().getName()).append("@").append(map.hashCode()).append("[");
		for (Map.Entry<String, String> entry : map.entrySet()) {
			sb.append("\n").append(entry.getKey()).append("=").append(entry.getValue());
		}
		return sb.append("\n]").toString();
	}

	/**
	 * 获取Map中的属性 该方法的参数适用于打印Map<String, byte[]>的情况
	 * 
	 * @return String key11=value11 \n key22=value22 \n key33=value33 \n......
	 */
	public static String getStringFromMapForByte(Map<String, byte[]> map) {
		StringBuilder sb = new StringBuilder();
		sb.append(map.getClass().getName()).append("@").append(map.hashCode()).append("[");
		for (Map.Entry<String, byte[]> entry : map.entrySet()) {
			sb.append("\n").append(entry.getKey()).append("=").append(new String(entry.getValue()));
		}
		return sb.append("\n]").toString();
	}

	/**
	 * 获取Map中的属性 该方法的参数适用于打印Map<String, Object>的情况
	 * 
	 * @return String key11=value11 \n key22=value22 \n key33=value33 \n......
	 */
	public static String getStringFromMapForObject(Map<String, Object> map) {
		StringBuilder sb = new StringBuilder();
		sb.append(map.getClass().getName()).append("@").append(map.hashCode()).append("[");
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			sb.append("\n").append(entry.getKey()).append("=").append(entry.getValue().toString());
		}
		return sb.append("\n]").toString();
	}
	
	/**   
	 * 按照key排序，用 java.util.TreeMap, 它将自动根据keys对Map进行排序
	 * @param unsortMap 需要排序的Map
	 * @return 排好序的Map
	 */
	public static Map<String, String> sortMapByTree(Map<String, String> unsortMap) {
		Map<String, String> treeMap = new TreeMap<String, String>(unsortMap);
		return treeMap;
	}
	
	/**   
	 * 按照key排序，用 java.util.TreeMap, 使用自定义的 Comparator进行降序排序
	 * @param unsortMap 需要排序的Map
	 * @return 排好序的Map
	 */
	public static Map<Integer, String> sortMapByComparator(Map<Integer, String> unsortMap) {
		Map<Integer, String> treeMap = new TreeMap<Integer, String>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.compareTo(o1);
			}
		});
		treeMap.putAll(unsortMap);
		return treeMap;
	}
	
	/**   
	 * 按照value排序
	 * @param unsortMap 需要排序的Map
	 * @return 排好序的Map
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> unsortMap) {

		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(unsortMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});
		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	
}
