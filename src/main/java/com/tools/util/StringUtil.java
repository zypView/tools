package com.tools.util;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.tools.util.rely.BCConvert;
import com.tools.util.rely.StringRely;

/**
 * 字符串工具类
 */
public class StringUtil {

	/**
	 * 获取系统流水号
	 * 
	 * @param length
	 *          指定流水号长度
	 * @param isNumber
	 *          指定流水号是否全由数字组成
	 */
	public static String getSysJournalNo(int length, boolean isNumber) {
		// replaceAll()之后返回的是一个由十六进制形式组成的且长度为32的字符串
		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		if (uuid.length() > length) {
			uuid = uuid.substring(0, length);
		} else if (uuid.length() < length) {
			for (int i = 0; i < length - uuid.length(); i++) {
				uuid = uuid + Math.round(Math.random() * 9);
			}
		}
		if (isNumber) {
			return uuid.replaceAll("a", "1").replaceAll("b", "2").replaceAll("c", "3").replaceAll("d", "4").replaceAll("e", "5").replaceAll("f", "6");
		} else {
			return uuid;
		}
	}

	/**
	 * 判断输入的字符串参数是否为空
	 * 
	 * @return boolean 空则返回true,非空则flase
	 */
	public static boolean isEmpty(String input) {
		return null == input || 0 == input.length() || 0 == input.replaceAll("\\s", "").length();
	}

	/**
	 * 判断输入的字节数组是否为空
	 * 
	 * @return boolean 空则返回true,非空则flase
	 */
	public static boolean isEmpty(byte[] bytes) {
		return null == bytes || 0 == bytes.length;
	}

	/**
	 * 字节数组转为字符串 如果系统不支持所传入的<code>charset</code>字符集,则按照系统默认字符集进行转换
	 * 
	 * @param data
	 *          需转换的字节数组
	 * @param charset
	 *          转码格式 如 ISO-8859-1
	 */
	public static String getString(byte[] data, String charset) {
		if (isEmpty(data)) {
			return "";
		}
		if (isEmpty(charset)) {
			return new String(data);
		}
		try {
			return new String(data, charset);
		} catch (UnsupportedEncodingException e) {
			return new String(data);
		}
	}

	/**
	 * 字符串转为字节数组 如果系统不支持所传入的<code>charset</code>字符集,则按照系统默认字符集进行转换
	 * 
	 * @param data
	 *          需转换的字节数组
	 * @param charset
	 *          转码格式 如 ISO-8859-1
	 */
	public static byte[] getBytes(String data, String charset) {
		data = (data == null ? "" : data);
		if (isEmpty(charset)) {
			return data.getBytes();
		}
		try {
			return data.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			return data.getBytes();
		}
	}

	/**
	 * HTML字符转义 对输入参数中的敏感字符进行过滤替换,防止用户利用JavaScript等方式输入恶意代码 String input = <img
	 * src='http://t1.baidu.com/it/fm=0&gp=0.jpg'/> HtmlUtils.htmlEscape(input);
	 * //from spring.jar StringEscapeUtils.escapeHtml(input); //from
	 * commons-lang.jar
	 * 尽管Spring和Apache都提供了字符转义的方法,但Apache的StringEscapeUtils功能要更强大一些
	 * StringEscapeUtils提供了对HTML,Java,JavaScript,SQL,XML等字符的转义和反转义
	 * 但二者在转义HTML字符时,都不会对单引号和空格进行转义,而本方法则提供了对它们的转义
	 * 
	 * @return String 过滤后的字符串
	 */
	public static String htmlEscape(String input) {
		if (isEmpty(input)) {
			return input;
		}
		input = input.replaceAll("&", "&amp;");
		input = input.replaceAll("<", "&lt;");
		input = input.replaceAll(">", "&gt;");
		input = input.replaceAll(" ", "&nbsp;");
		input = input.replaceAll("'", "&#39;"); // IE暂不支持单引号的实体名称,而支持单引号的实体编号,故单引号转义成实体编号,其它字符转义成实体名称
		input = input.replaceAll("\"", "&quot;"); // 双引号也需要转义，所以加一个斜线对其进行转义
		input = input.replaceAll("\n", "<br/>"); // 不能把\n的过滤放在前面，因为还要对<和>过滤，这样就会导致<br/>失效了
		return input;
	}

	/**
	 * 获取客户端真实IP地址
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 数字格式转换
	 * 
	 * @param number
	 *          需要转换的数字
	 * @param pattern
	 *          转换格式，例 #,###.00 || #,### || #,###.00%
	 */
	public static String formatDecimal(Double number, String pattern) {
		DecimalFormat df = new DecimalFormat(pattern);
		return df.format(number);
	}

	/**
	 * 数字格式转换
	 * 
	 * @param number
	 *          需要转换的数字
	 * @param pattern
	 *          转换格式，例 #,###.00 || #,### || #,###.00%
	 */
	public static String formatInteger(Integer number, String pattern) {
		DecimalFormat df = new DecimalFormat(pattern);
		return df.format(number);
	}

	/**
	 * 将整数count转化成num位的字符串位数不够就补零
	 */
	public static String countFormat(int count, int num) {
		String tmp = String.valueOf(count);
		int len = String.valueOf(count).length();
		if (len < num) {
			for (int i = 0; i < num - len; i++) {
				tmp = "0" + tmp;
			}
		}
		return tmp;
	}

	/**
	 * 传入一个小数输出一个相应的百分比格式，传入的那个数要求很高
	 */
	public static String getPercent(double src) {
		String dest = null;
		try {
			NumberFormat formater = NumberFormat.getPercentInstance();
			formater.setGroupingUsed(false);
			formater.setMaximumFractionDigits(2);
			dest = formater.format(src);
		} catch (Exception e) {
			dest = null;
		}
		return dest;
	}

	/**
	 * 传入一个路径，返回文件名
	 */
	public static String filenameOfPath(String path) {
		String temp[] = path.split("\\\\");
		String fileName = temp[temp.length - 1];
		return fileName;
	}

	/**
	 * 把string array or list用给定的符号symbol连接成一个字符串
	 * 
	 * @param list
	 *          需要处理的列表
	 * @param symbol
	 *          链接的符号
	 * @return 处理后的字符串
	 */
	public static String joinString(@SuppressWarnings("rawtypes") List list, String symbol) {
		String result = "";
		if (list != null) {
			for (Object o : list) {
				String temp = o.toString();
				if (temp.trim().length() > 0)
					result += (temp + symbol);
			}
			if (result.length() > 1) {
				result = result.substring(0, result.length() - 1);
			}
		}
		return result;
	}

	/**
	 * 判定第一个字符串是否等于的第二个字符串中的某一个值
	 * 
	 * @param str1
	 *          测试的字符串
	 * @param str2
	 *          字符串数组
	 * @param split
	 *          str2字符串的分隔符
	 * @return 是否包含
	 */
	public static boolean requals(String str1, String str2, String split) {
		if (str1 != null && str2 != null) {
			str2 = str2.replaceAll("\\s*", "");
			String[] arr = str2.split(split);
			for (String t : arr) {
				if (t.equals(str1.trim())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 隐藏邮件地址前缀。
	 * 
	 * @param email
	 *          - EMail邮箱地址 例如: ssss@koubei.com 等等...
	 * @return 返回已隐藏前缀邮件地址, 如 *********@koubei.com.
	 */
	public static String getHideEmailPrefix(String email) {
		if (null != email) {
			int index = email.lastIndexOf('@');
			if (index > 0) {
				email = repeat("*", index).concat(email.substring(index));
			}
		}
		return email;
	}

	/**
	 * repeat - 通过源字符串重复生成N次组成新的字符串。
	 * 
	 * @param src
	 *          - 源字符串 例如: 空格(" "), 星号("*"), "浙江" 等等...
	 * @param num
	 *          - 重复生成次数
	 * @return 返回已生成的重复字符串
	 */
	public static String repeat(String src, int num) {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < num; i++)
			s.append(src);
		return s.toString();
	}

	/**
	 * 全角字符变半角字符
	 * 
	 * @param str
	 *          需要处理的字符串
	 * @return 处理后的字符串
	 */
	public static String full2Half(String str) {
		if (isEmpty(str)) {
			return "";
		}
		return BCConvert.qj2bj(str);
	}

	/**
	 * 半角字符变全角字符
	 * 
	 * @param str
	 *          需要处理的字符串
	 * @return 处理后的字符串
	 */
	public static String Half2Full(String str) {
		if (isEmpty(str)) {
			return "";
		}
		return BCConvert.bj2qj(str);
	}

	/**
	 * 页面中去除字符串中的空格、回车、换行符、制表符
	 * 
	 * @param str
	 *          需要处理的字符串
	 */
	public static String replaceBlank(String str) {
		if (str != null) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			str = m.replaceAll("");
		}
		return str;
	}

	/**
	 * 判断字符串数组中是否包含某字符串元素
	 * 
	 * @param substring
	 *          某字符串
	 * @param source
	 *          源字符串数组
	 * @return 包含则返回true，否则返回false
	 */
	public static boolean isIn(String substring, String[] source) {
		if (isEmpty(substring) || !CheckUtil.valid(source)) {
			return false;
		}
		for (String t : source) {
			if (substring.equals(t)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 字符串转换unicode.实现native2ascii.exe类似的功能
	 *
	 * @param string
	 *          需要处理的字符串
	 */
	public static String string2Unicode(String string) {
		StringBuilder uni = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			String temp = "\\u" + String.valueOf(Integer.toHexString(string.charAt(i)));
			uni.append(temp);
		}
		return uni.toString();
	}

	/**
	 * 转字符串 实现native2ascii.exe类似的功能
	 *
	 * @param unicode
	 *          需要处理的字符串
	 */
	public static String unicode2String(String unicode) {
		StringBuilder str = new StringBuilder();
		String[] hex = unicode.split("\\\\u");
		for (int i = 1; i < hex.length; i++) {
			int data = Integer.parseInt(hex[i], 16);
			str.append((char) data);
		}
		return str.toString();
	}

	/**
	 * 删除所有的标点符号
	 *
	 * @param str
	 *          处理的字符串
	 */
	public static String trimPunct(String str) {
		if (isEmpty(str)) {
			return "";
		}
		return str.replaceAll("[\\pP\\p{Punct}]", "");
	}

	/**
	 * 字符串相似度比较(速度较快)
	 */
	public static double SimilarityRatio(String str1, String str2) {
		str1 = StringUtil.trimPunct(str1);
		str2 = StringUtil.trimPunct(str2);
		if (str1.length() > str2.length()) {
			return StringRely.SimilarityRatio(str1, str2);
		} else {
			return StringRely.SimilarityRatio(str2, str1);
		}
	}

	/**
	 * 字符串相似度比较(速度较快)
	 */
	public static double SimilarDegree(String str1, String str2) {
		str1 = StringUtil.trimPunct(str1);
		str2 = StringUtil.trimPunct(str2);
		if (str1.length() > str2.length()) {
			return StringRely.SimilarDegree(str1, str2);
		} else {
			return StringRely.SimilarDegree(str2, str1);
		}
	}

	/**
	 * 获取字符串str在String中出现的次数
	 *
	 * @param string
	 *          处理的字符串
	 * @param str
	 *          子字符串
	 */
	public static int countSubStr(String string, String str) {
		if ((str == null) || (str.length() == 0) || (string == null) || (string.length() == 0)) {
			return 0;
		}
		int count = 0;
		int index = 0;
		while ((index = string.indexOf(str, index)) != -1) {
			count++;
			index += str.length();
		}
		return count;
	}

	/**
	 * 替换第一次出现的子串
	 *
	 * @param s
	 *          原字符串内容
	 * @param sub
	 *          需要被替换的字符串内容
	 * @param with
	 *          替换成的字符串内容
	 */
	public static String replaceFirst(String s, String sub, String with) {
		int i = s.indexOf(sub);
		if (i == -1) {
			return s;
		}
		return s.substring(0, i) + with + s.substring(i + sub.length());
	}

	/**
	 * 替换最后一次出现的子字符串
	 *
	 * @param s
	 *          原字符串内容
	 * @param sub
	 *          需要被替换的字符串内容
	 * @param with
	 *          替换成的字符串内容
	 */
	public static String replaceLast(String s, String sub, String with) {
		int i = s.lastIndexOf(sub);
		if (i == -1) {
			return s;
		}
		return s.substring(0, i) + with + s.substring(i + sub.length());
	}

	/**
	 * 删除所有的子串
	 *
	 * @param s
	 *          原字符串
	 * @param sub
	 *          需要被删除的子串
	 */
	public static String remove(String s, String sub) {
		int c = 0;
		int sublen = sub.length();
		if (sublen == 0) {
			return s;
		}
		int i = s.indexOf(sub, c);
		if (i == -1) {
			return s;
		}
		StringBuilder sb = new StringBuilder(s.length());
		do {
			sb.append(s.substring(c, i));
			c = i + sublen;
		} while ((i = s.indexOf(sub, c)) != -1);
		if (c < s.length()) {
			sb.append(s.substring(c, s.length()));
		}
		return sb.toString();
	}
	
	/**
	 * 将字符串首字母转大写
	 * 
	 * @param str
	 *          需要处理的字符串
	 */
	public static String upperFirstChar(String str) {
		if (isEmpty(str)) {
			return "";
		}
		char[] cs = str.toCharArray();
		if ((cs[0] >= 'a') && (cs[0] <= 'z')) {
			cs[0] -= (char) 0x20;
		}
		return String.valueOf(cs);
	}

	/**
	 * 将字符串首字母转小写
	 * 
	 * @param str
	 */
	public static String lowerFirstChar(String str) {
		if (isEmpty(str)) {
			return "";
		}
		char[] cs = str.toCharArray();
		if ((cs[0] >= 'A') && (cs[0] <= 'Z')) {
			cs[0] += (char) 0x20;
		}
		return String.valueOf(cs);
	}

	/**
	 * 判读俩个字符串右侧的length个字符串是否一样
	 */
	public static boolean rigthEquals(String str1, String str2, int length) {
		return right(str1, length).equals(right(str2, length));
	}

	/**
	 * 判读俩个字符串左侧的length个字符串是否一样
	 */
	public static boolean leftEquals(String str1, String str2, int length) {
		return left(str1, length).equals(left(str2, length));
	}
	
	/**
	 * 截取字符串左侧指定长度的字符串
	 *
	 * @param input
	 *          输入字符串
	 * @param count
	 *          截取长度
	 * @return 截取字符串
	 */
	public static String left(String input, int count) {
		if (isEmpty(input)) {
			return "";
		}
		count = (count > input.length()) ? input.length() : count;
		return input.substring(0, count);
	}

	/**
	 * 截取字符串右侧指定长度的字符串
	 *
	 * @param input
	 *          输入字符串
	 * @param count
	 *          截取长度
	 * @return 截取字符串 Summary 其他编码的有待测试
	 */
	public static String right(String input, int count) {
		if (isEmpty(input)) {
			return "";
		}
		count = (count > input.length()) ? input.length() : count;
		return input.substring(input.length() - count, input.length());
	}
}
